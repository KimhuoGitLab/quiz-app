import React, { Component } from 'react';
import {
  Container,
  Button,
  Content,
  Body,
  List,
  ListItem,
  CheckBox,
} from "native-base";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
} from 'react-native';
import data from './data.json';

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: data.questions,
      selectedIndex: [],
      totalCorrect: []
    }
  }

  componentDidMount() {
    // console.log(data)
  }

  _onSubmit() {
    if (this.state.selectedIndex.length < this.state.data.length) {
      this._onRequire()
    } else {
      var totalQuestin = this.state.data.length
      var result = (this.state.totalCorrect.length * 100) / totalQuestin

      Alert.alert(
        'Result',
        'Your percentage of correctly answered questions is ' + result + '%',
        [
          {
            text: 'OK',
            onPress: () => console.log('OK Pressed')
          }
        ],
        { cancelable: false }
      )
    }
  }

  _onRequire() {
    Alert.alert(
      'Require!',
      'Please, answer to all the questions first before you submit.',
      [
        {
          text: 'OK',
          onPress: () => console.log('OK Pressed')
        }
      ],
      { cancelable: false }
    )
  }

  _checking(clickIndex, clickItem) {
    // checking to return true or false from selected answers from state
    var check = false
    this.state.selectedIndex.map((item, index) => {
      if (item.main_index == clickIndex && item.answer == clickItem) {
        check = true
      }
    })

    return check
  }

  render() {
    return (
      <Container style={{ padding: 3, paddingTop: 30 }}>
        <Content>
          <Text style={styles.welcome}>
            Welcome to Quiz App!
          </Text>
          <Text style={styles.instructions}>
            Please, choose one of answers of the following questions.
          </Text>
          <List>
            {
              this.state.data.map((main_item, main_index) => (
                <ListItem key={main_index}>
                  <Body>
                    <Text style={{ fontSize: 16, color: "#d35400" }}>{main_index + 1}{'.  '}{main_item.text}</Text>
                    {
                      main_item.answers.map((item, index) => (
                        <TouchableOpacity key={index} style={{ flex: 1, flexDirection: "row", marginTop: 15 }} hitSlop={{ top: 16, left: 16, bottom: 16, right: 0 }}
                          onPress={() => {
                            // clear all selected answers from this index question
                            var check = this.state.selectedIndex.filter((el) => {
                              return el.main_index !== main_index;
                            });
                            // set new value to state
                            this.setState({
                              selectedIndex: check
                            })
                            // get new selected answer from this index question
                            var data = { main_index: main_index, answer: item }
                            // push new selected answer to state from this index question
                            this.setState(prevState => ({
                              selectedIndex: [...prevState.selectedIndex, data]
                            }))

                            // clear all selected answers from this index question
                            var correct = this.state.totalCorrect.filter((el) => {
                              return el.main_index !== main_index;
                            });
                            // set new value to state
                            this.setState({
                              totalCorrect: correct
                            })
                            // set score for the correct answer
                            if (index == main_item.correct) {
                              // get new selected answer from this index question
                              var data = { main_index: main_index, answer: item }
                              // push new selected answer to state from this index question
                              this.setState(prevState => ({
                                totalCorrect: [...prevState.totalCorrect, data]
                              }))
                            }

                            // setTimeout(() => {
                            //   console.log(this.state.totalCorrect)
                            // }, 500);
                          }}
                        >
                          <CheckBox checked={this._checking(main_index, item)} /><Text style={{ marginLeft: 16, fontSize: 14 }}>{item}</Text>
                        </TouchableOpacity>
                      ))
                    }
                  </Body>
                </ListItem>
              ))
            }
          </List>
          <Button
            onPress={() => this._onSubmit()}
            style={{ backgroundColor: "#00aeef", marginTop: 20, alignSelf: "center" }}
          >
            <Text style={{ marginLeft: 64, marginRight: 64, color: "white", fontSize: 16 }}>
              Submit
          </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    fontSize: 16,
  },
});
